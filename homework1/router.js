const greetingsController = require('./greetings/controller');
const {statusCode} = require('./config');

const notFound = res => {
  res.writeHead(statusCode.notFound)
  res.end()
};

module.exports = {
  post: {
    'hello': greetingsController
  },
  notFound
};