const statusCodes = {
  statusCode: {
    ok: '200',
    notFound: '404'
  }
}

const config = {
  'production': {
    httpPort: 3000,
    envName: 'production',
  },
  'development': {
    httpPort: 5000,
    envName: 'development'
  }
}

const environment = process.env.NODE_ENV ? String(process.env.NODE_ENV) : 'development'

module.exports = { ...statusCodes, ...config[environment] }