const payload = require('./payload');
const greetingsAction = require('./greetingsAction');
const {statusCode} = require('../config');
const {parseData} = require('../utils');

module.exports = (req, res) => {
  parseData(req, (data) => {
    res.writeHead(statusCode.ok, {
      'Content-Type': 'application/json'
    });
    res.end(payload(greetingsAction.greetings(data)));
  })
}