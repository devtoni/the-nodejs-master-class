const http = require('http');
const router = require('./router');
const {parsePathName} = require('./utils');

const executeController = (req, res) => {
  const {method: httpMethod} = req
  router[httpMethod.toLowerCase()][parsePathName(req)].call(null, req, res)
}

module.exports = http.createServer((req, res) => {
  try {
    executeController(req, res)
  } catch (e) {
    router.notFound(res)
  }
})

