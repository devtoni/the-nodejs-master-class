const url = require('url');
const {StringDecoder} = require('string_decoder');

const parseUrl = reqUrl => url.parse(reqUrl, true);

module.exports = {
  parsePathName: ({url}) => {
    const path = parseUrl(url).pathname;
    return path.replace(/^\/+|\/+$/g, '');
  },
  parseData: (req, callback) => {
    const stringDecoder = new StringDecoder('utf-8');
    let buffer = '';

    req.on('data', data => {
      buffer += stringDecoder.write(data);
    })

    req.on('end', () => {
      buffer += stringDecoder.end();
      callback(buffer);
    })
  }
}