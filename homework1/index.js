const {httpPort, envName} = require('./config');
const httpServer = require('./app');

httpServer.listen(httpPort, () => {
  console.log(`Listening on port ${httpPort} on ${envName} environment`);
})